VideoJS.DOMReady(function(){
    var myManyPlayers = VideoJS.setup("All", {
        controlsBelow: false, // Display control bar below video instead of in front of
        controlsHiding: false, // Hide controls when mouse is not over the video
        defaultVolume: 0.85, // Will be overridden by user\'s last volume if available
        flashVersion: 9, // Required flash version for fallback
        linksHiding: true // Hide download links when video is supported});
    });
});